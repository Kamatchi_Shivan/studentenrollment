const express = require('express')
const router = express.Router()
const Student = require('../models/student')

var userId=0;

//creating
router.post('/add',async (req,resp) => {
    let student = new Student ({
        Name: req.body.Name,
        DOB: req.body.DOB,
        Gender: req.body.Gender,
        FatherName: req.body.FatherName,
        MotherName: req.body.MotherName,
        Address: req.body.Address,
        userId: await getuserId()
    })
  
function getuserId()
{
    var seq=1;
    userId=userId+seq++; 
    var userStuId='2019STU'+userId;
    return userStuId;
}


    try{
        let newStudent = await student.save()
        resp.status(200).json(newStudent)
    }
    catch(err)
    {
        resp.status(400).json({ message: err.message })
    }

})

//Getting id
router.get('/:id', getStudent,(req,resp) => {
    //console.log(student);
    resp.send(resp.student);
})

async function getStudent(req, resp, next) {
    let student
    try{
       student = await Student.findById(req.params.id).exec();
       //student = await Student.findById('5cfa6d9ced2efa2fb808ff59') 
      
       if(student==null)
       {
           return resp.status(404).json({message:'Cannot find Student'})
           //console.log(student);
       }
    }
    catch(err){
        return resp.status(500).json({message:err.message})
    }
    resp.student = student
    next()
}



module.exports = router