import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginModule } from './login.module';
import { LoginComponent } from './login.component';

function getHTMLInputElement(
  elem: HTMLElement,
  elQuery: string
): HTMLInputElement {
  return elem.querySelector(elQuery) as HTMLInputElement ;
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let nativeElem: HTMLElement;
  let pwd: HTMLInputElement;
  let username: HTMLInputElement;
  let loginbtn: HTMLButtonElement;
  let cancelbtn: HTMLButtonElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        LoginModule
        ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    nativeElem = fixture.debugElement.nativeElement;
  });

  it('should create an instance Login', () => {
    expect(component).toBeTruthy();
  });

  it('should contain textbox element as unameinput', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      username = getHTMLInputElement(nativeElem, 'input[name*="uname"]');
      expect(username).toBeTruthy();
    });
  });

  it('should contain textbox element as upwdinput', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      pwd = getHTMLInputElement(
        nativeElem,
        'input[name*="upwd"]'
      );
      expect( pwd).toBeTruthy();
    });
  });
  it('should contain the login button', async () => {
    loginbtn = (
    nativeElem.querySelector('button[type*="submit"]') as HTMLButtonElement
    );
    expect(loginbtn).toBeTruthy();
  });
  it('should contain the cancel button', async () => {
    cancelbtn = (
    nativeElem.querySelector('button[type*="reset"]') as HTMLButtonElement
    );
    expect(cancelbtn).toBeTruthy();
  });

  it('login button should contain text "Login"', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      loginbtn = (
        nativeElem.querySelector('button[type*="submit"]') as HTMLButtonElement
      );
      expect(loginbtn.textContent).toContain('Login');
    });
  });
  it('cancel button should contain text "cancel"', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      cancelbtn = (
        nativeElem.querySelector('button[type*="reset"]') as HTMLButtonElement
      );
      expect(cancelbtn.textContent).toContain('cancel');
    });
  });

  it('Enter the user id ,Password with incorrect credentials', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.loginUser({
        username: 'anitha',
        password: 'abcgmail'
      });
      expect(component.loginUser).toBeFalsy();
    });
  });

  it('Enter the user id ,Password with correct credentials', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.loginUser({
        uname: 'admin',
        upwd: 'admin'
      });
      expect(component.loginUser).toBeTruthy();
    });
  });

  it('Login with incorrect credentials ,it should throw invalid credential error', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.loginUser({
      uname: 'anitha',
        upwd: 'abcgmailcom'
      });
      component. ngOnInit();
      expect(component.loginUser).toBeFalsy();
    });
  });

  it('Login with incorrect credentials- space ,it should throw invalid credential error', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.loginUser({
      uname: '   ',
        upwd: '   '
      });
      component. ngOnInit();
      expect(component.loginUser).toBeFalsy();
    });
  });

  it('Login with incorrect credentials- special char,it should throw invalid credential error', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.loginUser({
      uname: 'admin@',
        upwd: '#$@%$%'
      });
      component. ngOnInit();
      expect(component.loginUser).toBeFalsy();
    });
  });

  it('Login with incorrect credentials - special char & space ,it should throw invalid credential error', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.loginUser({
      uname: 'an  it &&ha',
        upwd: 'admin %%'
      });
      component. ngOnInit();
      expect(component.loginUser).toBeFalsy();
    });
  });


});
