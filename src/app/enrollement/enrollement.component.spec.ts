import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollementComponent } from './enrollement.component';
import { ok } from 'assert';
function getHTMLInputElement(
  elem: HTMLElement,
  elQuery: string
): HTMLInputElement {
  return elem.querySelector(elQuery) as HTMLInputElement ;
}

describe('EnrollementComponent', () => {
  let component: EnrollementComponent;
  let fixture: ComponentFixture<EnrollementComponent>;
  let nativeElem: HTMLElement;
  let name: HTMLInputElement;
  let fname: HTMLInputElement;
  let mname: HTMLButtonElement;
  let address: HTMLButtonElement;
  let genbtn: HTMLButtonElement;
  let enrollbtn: HTMLButtonElement;
  let cancelbtn: HTMLButtonElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('form invalid when empty', () => {
    expect(component.registerForm.valid).toBeFalsy();
});

/*
  it('name field should not be empty', async () => {
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    component.registerForm.patchValue({ name: '' });
    expect(component.registerForm.controls.name.valid).toBeTruthy();
  });
});*/


  it('should contain textbox element as  Name', async () => {
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    name = getHTMLInputElement(nativeElem, 'input[name*="Name"]'); // input[name*="uname"]
    expect(name).toBeTruthy();
  });
});

  it('should contain textbox element as Father name', async () => {
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    fname = getHTMLInputElement(nativeElem, 'input[name*="FatherName"]'); // input[name*="uname"]
    expect(fname).toBeTruthy();
  });
});

  it('should contain textbox element as Mother name', async () => {
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    mname = getHTMLInputElement(nativeElem, 'input[name*="MotherName"]'); // input[name*="uname"]
    expect(mname).toBeTruthy();
  });
});

// gender - Male

  it('should contain button element as gender - Male', async () => {
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    genbtn = getHTMLInputElement(nativeElem, 'input[value*="Male"]'); // input[name*="uname"]
    // expect(genbtn.getAttribute('value')).toEqual('Male');
    expect(genbtn).toBeTruthy();
  });
});

// gender - FeMale

  it('should contain button element as gender - Male', async () => {
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    genbtn = getHTMLInputElement(nativeElem, 'input[value*="Female"]'); // input[name*="uname"]
    // expect(genbtn.getAttribute('value')).toEqual('Female');
    expect(genbtn).toBeTruthy();
  });
});

// address
  it('should contain textbox element as Address', async () => {
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    address = getHTMLInputElement(nativeElem, 'input[name*="Address"]'); // input[name*="uname"]
    expect(address).toBeTruthy();
  });
});
});
