import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService} from '../service/alert.service';
import { EnrollService} from '../service/enroll.service';

@Component({
  selector: 'app-enrollement',
  templateUrl: './enrollement.component.html',
  styleUrls: ['./enrollement.component.css']
})
export class EnrollementComponent implements OnInit {
enrollForm: FormGroup;
submitted =  false;
constructor(private route: ActivatedRoute,
            private formBuilder: FormBuilder,
            private router: Router,
            private enrollService: EnrollService,
            private alertService: AlertService) {
            this.createForm();
        }
        createForm() {
          this.enrollForm = this.formBuilder.group({
              Name: ['', Validators.required],
              DOB: ['', Validators.required],
              Gender: [''],
              FatherName: ['', Validators.required],
              MotherName: ['', Validators.required],
              Address: ['']
          });
       }

       addStudentt(Name, DOB, Gender, FatherName, MotherName, Address) {
         this.submitted = true,
        this.enrollService.addStudentt(Name, DOB, Gender, FatherName, MotherName, Address),
        this.enrollForm.reset();
     }
     ngOnInit() {
     }
     get f() { return this.enrollForm.controls; }

    }
