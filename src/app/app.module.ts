import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { NgModule, Component } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {RouterModule, Routes} from '@angular/router';
import { EnrollementComponent } from './enrollement/enrollement.component';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlertService} from './service/alert.service';
import { EnrollService} from './service/enroll.service';


const appRoutes: Routes = [
  {
    path: '',
    component: LoginComponent

  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'enrollement', component: EnrollementComponent
  },
  {
    path: 'enquiry', component: EnquiryComponent
  }

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    EnrollementComponent,
    EnquiryComponent
  ],
  imports: [
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    AlertService,
    EnrollService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
