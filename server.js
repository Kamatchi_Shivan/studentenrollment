const express = require('express')
const bodyParser= require('body-parser')
cors = require('cors')
const app= express()

const mongoose= require('mongoose')
// var autoIncrement = require("mongodb-autoincrement");

mongoose.connect('mongodb://localhost:27017/student',{ useNewUrlParser: true })
mongoose.Promise=global.Promise;
/*let studentSchema = require('../StudentsEnroll/models/student')
app.use('/student',studentSchema)*/
const db=mongoose.connection
db.on('error',(error)=>console.error(error))
db.once('open',()=> console.log('Connected to Database'))
/*mongoose.connect('mongodb://localhost:27017/student', function (err, db){
    autoIncrement.getNextSequence(db, students, function (err, autoIndex){
        var collection = db.collection(students);
        collection.insert({
            _id: autoIndex
        });
    });
});*/

app.use(bodyParser.json())
app.use(express.json())
app.use(cors())
const studentsRouter = require('./routes/students')
app.use('/students',studentsRouter)

app.listen(4000,() => console.log('Server Started'))


